"use client";

import { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import Profile from "@components/Profile";

const ProfilePage = () => {

    const [posts, setPosts] = useState([]);
    const {data : session} = useSession();
    const router = useRouter();

    const fetchPosts = async () => {
        const response = await fetch(`api/users/${session?.user.id}/posts`);
        const data = await response.json();
        console.log("posts in profile page", data);
        setPosts(data);
    }

    useEffect(() => {
        console.log("session in profile page ",session?.user._id)
            fetchPosts();
    }, []);

    const handleEdit =  (post) => {
        console.log("Handle edit in profile ", post);
        router.push(`/update-prompt?id=${post._id}`)
    }

    const handleDelete = async (post) => {
        console.log("Delete in profile page", post);
        const hasConfirmed = confirm("Are you sure you want to delete this prompt!");
        if(hasConfirmed){
            await fetch(`api/prompt/${post._id}`,{
                method: 'DELETE'
            })
            const filteredPosts = posts.filter( (p) => p._id != post._id);
            setPosts(filteredPosts);
        }
    }
    return (
        <Profile
            name="My"
            desc="Welcome to your personalized profile page"
            data={posts}
            handleEdit={handleEdit}
            handleDelete={handleDelete} />
    )
}

export default ProfilePage